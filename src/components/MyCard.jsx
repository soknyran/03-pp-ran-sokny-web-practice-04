import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";

class MyCard extends Component {
    constructor() {
        super();
        this.state = {
            person: [
                {
                    id: 1,
                    image:"https://www.codingdojo.com/blog/wp-content/uploads/Screen-Shot-2020-01-08-at-3.52.51-PM.png",
                    title: "Aliens",
                    discription:"Some quick example text to build on the card title and make up the bulk ofthe card's content.",
                },
                {
                    id: 2,
                    image:"https://www.northeastern.edu/graduate/blog/wp-content/uploads/2020/06/iStock-1157345255.jpg",
                    title: "Thayuth",
                    discription:"Some quick example text to build on the card title and make up the bulk ofthe card's content.",
                },
                {
                    id: 3,
                    image:"https://www.codingdojo.com/blog/wp-content/uploads/Screen-Shot-2020-01-08-at-3.52.51-PM.png",
                    title: "Sothea",
                    discription:"Some quick example text to build on the card title and make up the bulk ofthe card's content.",
                },
            ]
        }
    }

    onDelete = (e) => {
        console.log("E", e.target.name);
        let temp = this.state.person.filter(item =>{
          return item.id != e.target.name
        })
        this.setState({
          person: temp
        },()=>{
          console.log(this.state.person);
        });
    };
    
    render() {
        let myCard = this.state.person.map((item, index) => {
            console.log("ITEM:", item);
            return (
                <div key={index}>
                    <Card style={{ width: "18rem" }}>
                        <Card.Img variant="top" src={item.image} />
                        <Card.Body>
                            <Card.Title>{item.title}</Card.Title>
                            <Card.Text>{item.discription}</Card.Text>
                            <Button name={item.id} onClick={this.onDelete} variant="primary">Delete</Button>
                        </Card.Body>
                    </Card>
                </div>
            );
        });

        return <div>{myCard}</div>;
    }
}

export default MyCard;

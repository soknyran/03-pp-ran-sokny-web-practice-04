import React from 'react'
import './styles.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavMenu from "./components/NavMenu"
import MyCard from './components/MyCard';


function App() {
    return(
        <div className="App">
            <NavMenu/>
            <MyCard/>
        </div>
    );
}

export default App;